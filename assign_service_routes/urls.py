from django.urls import include
from django.contrib import admin
from django.urls import path
from users.views import Login
from users.views import Logout


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', Login.as_view(), name="login"),
    path('logout/', Logout.as_view(), name="logout"),
    path('routes/', include('routes.urls', namespace="routes")),
    path('services/', include('services.urls', namespace="services")),
]
