from django.contrib import admin
from clients.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('user', 'address', 'latitude', 'longitude')
    search_fields = ('user__first_name', 'user__last_name')
    list_filter = ('user__first_name', 'user__last_name')
    ordering = ('user__first_name', 'user__last_name')
