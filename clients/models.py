from django.db import models
from users.models import User


class Client(models.Model):
    """ Clients Model"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    latitude = models.CharField(max_length=15)
    longitude = models.CharField(max_length=15)
    address = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        """Return username."""
        return "%s %s" % (self.user.first_name, self.user.last_name)

    class Meta:
        verbose_name_plural = "Clients"
        verbose_name = "Client"
        ordering = ['user__first_name']
