# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from services.models import Service


class ServiceForm(forms.ModelForm):
    class Meta:
        model = Service
        fields = ('type', 'client', 'demand', 'state', 'detail')

    def __init__(self, *args, **kwargs):
        super(ServiceForm, self).__init__(*args, **kwargs)

        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})
