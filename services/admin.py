from django.contrib import admin
from services.models import TypeService
from services.models import Service


@admin.register(TypeService)
class TypeServiceAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'duration')
    search_fields = ('name',)
    list_filter = ('name',)
    ordering = ('name',)


@admin.register(Service)
class ServiceAdmin(admin.ModelAdmin):
    list_display = ('type', 'detail', 'client', 'created', 'demand', 'state')
    search_fields = ('type__name', 'client__user__first_name',  'client__user__last_name', 'state')
    list_filter = ('type__name', 'client__user__first_name',  'client__user__last_name', 'state')
    fields = ('type', 'detail', 'client', 'demand', 'state')
    ordering = ('-demand', '-created')