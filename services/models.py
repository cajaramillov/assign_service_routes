from django.db import models
from clients.models import Client
from django.utils.translation import ugettext_lazy as _


STATE_SERVICE_CHOICES = (
    ('P', _(u"Pending")),
    ('A', _(u"Attending")),
    ('F', _(u"Finished")),
)


class TypeService(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True, null=True)
    duration = models.PositiveIntegerField(default=0)

    def __str__(self):
        """Return username."""
        return self.name

    class Meta:
        verbose_name_plural = "Type Services"
        verbose_name = "Type Service"


class Service(models.Model):
    type = models.ForeignKey(TypeService, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the service was created.'
    )
    demand = models.DateTimeField(
        'demand service at',
        help_text='Date time on which the object was last modified.'
    )
    state = models.CharField(max_length=1, choices=STATE_SERVICE_CHOICES, default='P')
    detail = models.TextField(blank=True, null=True)

    def __str__(self):
        """Return username."""
        return "%s-%s %s" % (self.type.name, self.client.user.first_name, self.client.user.last_name)

    class Meta:
        verbose_name_plural = "Services"
        verbose_name = "Service"
        ordering = ['-demand', '-created']
