from django.urls import path
from services.views import ListServices
from services.views import AddService

app_name = 'services'

urlpatterns = [
    path('', ListServices.as_view(), name="list_services"),
    path('add-service/', AddService.as_view(), name="add_service"),
]