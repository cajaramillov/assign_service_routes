from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.base import View
from services.models import Service
from datetime import datetime
from django.utils.timezone import get_current_timezone
from services.forms import ServiceForm
from django.contrib import messages
from routes.create_routes import create_routes


class ListServices(LoginRequiredMixin, View):
    login_url = '/'
    template_name = 'services/services.html'

    def get(self, request):
        date_now = datetime.now(tz=get_current_timezone())
        services = Service.objects.filter(demand__year=date_now.year,
                                          demand__month=date_now.month,
                                          demand__day=date_now.day)
        return render(request,
                      self.template_name,
                      {'services': services})


class AddService(LoginRequiredMixin, View):
    login_url = '/'
    form_class = ServiceForm
    template_name = 'services/add_service.html'

    def get(self, request):
        form = self.form_class()
        return render(request, self.template_name, {'form': form})

    def post(self, request):
        post = request.POST.copy()
        post['demand'] = str(post['demand']).replace("T", " ")
        date_service = datetime.strptime(str(post['demand']), '%Y-%m-%d %H:%M')
        date_now = datetime.now(date_service.tzinfo)

        if date_service > date_now:
            form = self.form_class(post)

            if form.is_valid():
                form.save()
                messages.add_message(request, messages.INFO, 'The Service was added successfully')

                if date_service.year == date_now.year and date_service.month == date_now.month and date_service.day == date_now.day:
                    create_routes()

            else:
                messages.add_message(request, messages.ERROR, 'The Service could not be added')

        else:
            messages.add_message(request, messages.ERROR, 'Services cannot be created on dates before the current one')

        return self.get(request)