from django.shortcuts import render
from django.views.generic.base import View
from django.contrib import auth
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect


class Login(View):
    template_name = 'users/login.html'

    def get(self, request):
        return render(request, self.template_name)

    def post(self, request):
        username = request.POST["signin_username"]
        password = request.POST["signin_password"]
        user = auth.authenticate(username=username,
                                 password=password)

        if user is not None and user.is_active:
            auth.login(request, user)

            if user.rol_user == "AD":
                return HttpResponseRedirect(reverse('routes:list_routes'))

            else:
                messages.add_message(request, messages.ERROR, "The User is not assigned a Role")

        else:
            messages.add_message(request, messages.ERROR, "The user does not exist in the System")

        return self.get(request)


class Logout(View):

    def get(self, request):
        auth.logout(request)
        return HttpResponseRedirect(reverse('login'))