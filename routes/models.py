from django.db import models
from technicians.models import Technician
from services.models import Service


class Route(models.Model):
    technician = models.ForeignKey(Technician, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    created = models.DateTimeField(
        'created at',
        auto_now_add=True,
        help_text='Date time on which the route was created.'
    )

    def __str__(self):
        """Return username."""
        return "%s-%s %s" % (self.service.type.name, self.technician.user.first_name, self.technician.user.last_name)

    class Meta:
        verbose_name_plural = "Routes"
        verbose_name = "Route"
        ordering = ['-created']
