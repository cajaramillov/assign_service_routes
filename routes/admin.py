from django.contrib import admin
from routes.models import Route


@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ('technician', 'service', 'created')
    search_fields = ('service__type__name', 'technician__user__first_name',  'technician__user__last_name')
    list_filter = ('service__type__name', 'technician__user__first_name',  'technician__user__last_name')
    ordering = ('-created',)
