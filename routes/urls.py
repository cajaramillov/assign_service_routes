from django.urls import path
from routes.views import ListRoutes
from routes.views import AttendService

app_name = 'routes'

urlpatterns = [
    path('', ListRoutes.as_view(), name="list_routes"),
    path('attend-service/<int:code_route>/', AttendService.as_view(), name="attend_service"),
]