from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic.base import View

from services.models import Service
from technicians.models import Technician
from datetime import datetime
from django.utils.timezone import get_current_timezone
from routes.models import Route
from django.contrib import messages


class ListRoutes(LoginRequiredMixin, View):
    login_url = '/'
    template_name = 'routes/routes.html'

    def get(self, request):
        date_now = datetime.now(tz=get_current_timezone())
        print(date_now)
        data_technicians = []
        technicians = Technician.objects.all()

        for technician in technicians:
            data_technician = {}
            routes = Route.objects.filter(technician=technician.id,
                                          created__year=date_now.year,
                                          created__month=date_now.month,
                                          created__day=date_now.day).order_by('service__demand')
            if len(routes) > 0:
                data_technician['id'] = technician.id
                data_technician['first_name'] = technician.user.first_name
                data_technician['routes'] = routes
                data_technicians.append(data_technician)

        print(data_technicians)
        return render(request,
                      self.template_name,
                      {'technicians': data_technicians})


class AttendService(LoginRequiredMixin, View):
    login_url = '/'

    def get(self, request, code_route):
        try:
            route = Route.objects.get(pk=code_route)
            service = Service.objects.get(pk=route.service.pk)
            service.state = 'F'
            service.save()
            messages.add_message(request, messages.INFO, "The service was attended successfully")

        except Route.DoesNotExist:
            messages.add_message(request, messages.ERROR, "The route does not exist")

        except Service.DoesNotExist:
            messages.add_message(request, messages.ERROR, "The service does not exist")

        list_routes = ListRoutes()
        return list_routes.get(request)
