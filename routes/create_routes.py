from technicians.models import Technician
from services.models import Service
from routes.models import Route
from multiprocessing import Process
from multiprocessing import Queue
from multiprocessing import cpu_count
from queue import Empty
from datetime import timedelta
from datetime import datetime
from django.utils.timezone import get_current_timezone
from haversine import haversine
from haversine import Unit


def search_last_rute(service, technician):
    date_now = datetime.now(tz=get_current_timezone())
    previous_services = Route.objects.filter(technician=technician.id,
                                             created__year=date_now.year,
                                             created__month=date_now.month,
                                             created__day=date_now.day,
                                             service__demand__lte=service.demand).order_by('-service__demand')
    if len(previous_services) > 0:
        return previous_services[0]
    return None


def search_current_technician_position(service, technician):
    last_route = search_last_rute(service, technician)

    if last_route is not None:
        latitude = last_route.service.client.latitude
        longitude = last_route.service.client.longitude
        return [latitude, longitude]
    return None


def evaluate_technician_availability(service, technician):
    last_route = search_last_rute(service, technician)

    if last_route is not None:
        time_delta = timedelta(minutes=last_route.service.type.duration)
        time_service = last_route.service.demand + time_delta

        if time_service > service.demand:
            return False

    return True


def find_nearest_technician(service, technicians):
    technician_nearest = None

    try:
        point_service = (float(service.client.latitude), float(service.client.longitude))
        min_distance = float('inf')

        for technician in technicians:
            last_service_position = search_current_technician_position(service, technician)

            if last_service_position is not None:
                point_technician = (float(last_service_position[0]), float(last_service_position[1]))
            else:
                point_technician = (float(technician.latitude), float(technician.longitude))

            distance = haversine(point_technician, point_service, unit=Unit.METERS)

            if distance < min_distance and evaluate_technician_availability(service, technician):
                min_distance = distance
                technician_nearest = technician
                print(technician)

        return technician_nearest

    except ValueError:
        return None


def add_service_route_technician(service, technician):
    route = Route(technician=technician,
                  service=service)
    route.save()


def worker_routes(queue_services):
    technicians = Technician.objects.all()

    while True:
        try:
            service = queue_services.get_nowait()
            technician_nearest = find_nearest_technician(service, technicians)

            if technician_nearest is not None:
                add_service_route_technician(service, technician_nearest)

        except Empty:
            break


def create_routes():
    technicians = Technician.objects.all()

    if len(technicians) > 0:
        cpus = cpu_count()
        print("#CPUs: ", cpus)
        date_now = datetime.now(tz=get_current_timezone())
        services = Service.objects.filter(demand__year=date_now.year,
                                          demand__month=date_now.month,
                                          demand__day=date_now.day,
                                          state__iexact='P').order_by('demand')

        Route.objects.filter(service__demand__gte=date_now).delete()
        processes = []
        print(services)

        if len(services) > 0:
            queue_services = Queue(len(services))

            for service in services:
                queue_services.put(service)

            #print("Queue Init ", queue_services.)

            '''p = Process(target=worker_routes, args=(queue_services,))
            p.start()
            p.join()'''

            for i in range(cpus):
                processes.append(Process(target=worker_routes, args=(queue_services,)))

            for process in processes:
                process.start()

            for process in processes:
                process.join()
