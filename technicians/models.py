from django.db import models
from users.models import User


class Technician(models.Model):
    """ Clients Model"""
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    latitude = models.CharField(max_length=15)
    longitude = models.CharField(max_length=15)
    job_title = models.CharField(max_length=100)

    def __str__(self):
        """Return username."""
        return "%s %s" % (self.user.first_name, self.user.last_name)

    class Meta:
        verbose_name_plural = "Technicians"
        verbose_name = "Technician"
        ordering = ['user__first_name']
