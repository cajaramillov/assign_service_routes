from django.contrib import admin
from technicians.models import Technician


@admin.register(Technician)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('user', 'job_title', 'latitude', 'longitude')
    search_fields = ('user__first_name', 'user__last_name', 'job_title')
    list_filter = ('user__first_name', 'user__last_name', 'job_title')
    ordering = ('user__first_name', 'user__last_name')

